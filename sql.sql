create table company(
eName varchar(20),
empId int(13) ,
empNo int(4),
salary int(5),
contact int(10));
INSERT INTO company VALUES
  ('SANTHOSH',40004,102,2500,987654320),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321);
 show tables;
 create table CodeBoard(
eName varchar(20),
empId int(13) ,
empNo int(4),
salary int(5),
contact int(10));
INSERT INTO codeboard VALUES
  ('Periasamy',1000,100,10000,861007425),
 ('Gokul',1001,101,25000,987654321),
 ('GunaSeelan',1002,102,32000,987654321),
 ('ArunPandiyan',1003,103,25500,987654321),
 ('Ayyapan',1004,104,35000,987654321);
select * from codeboard;
commit;
 UPDATE codeBoard 
SET 
    contact = 904704767
WHERE
    empId = 1001;
commit; 
desc codeboard;
select * from codeboard;
select eName,empId,empNo,contact,salary,salary*12 as 'AnnualSalary' from codeboard;
select salary||''||contact  from codeboard;
select concat(empId,empNo) as 'joins' from codeboard;

alter table codeboard add designation varchar(40);
update codeboard set designation="developer" where empNo in(100,101,102,103,104);
 
 select max(salary) from codeboard;
 select min(salary) from codeboard;
 select avg(salary) from codeboard;
 select count(salary) from codeboard;
 commit;
  rollback;
 alter table codeboard drop  designation;
 rollback;
 SELECT 
    *
FROM
    codeboard;
 rollback;
select * from codeboard order by salary desc limit 3;

select * from codeboard order by empNo desc limit 3;

commit;
select * from company;
delete from company;
rollback;

drop table company;

create table company(
eName varchar(20),
empId int(13) ,
empNo int(4),
salary int(5),
contact int(10));
INSERT INTO company VALUES
  ('SANTHOSH',40004,102,2500,987654320),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321),
 ('SANTHOSH',40004,102,2500,987654321);
 show tables;
 create table CodeBoard(
eName varchar(20),
empId int(13) ,
empNo int(4),
salary int(5),
contact int(10));
INSERT INTO codeboard VALUES
  ('Periasamy',1000,100,10000,861007425),
 ('Gokul',1001,101,25000,987654321),
 ('GunaSeelan',1002,102,32000,987654321),
 ('ArunPandiyan',1003,103,25500,987654321),
 ('Ayyapan',1004,104,35000,987654321);
select * from codeboard;
commit;
 UPDATE codeBoard 
SET 
    contact = 904704767
WHERE
    empId = 1001;
commit; 
desc codeboard;
select * from codeboard;
select eName,empId,empNo,contact,salary,salary*12 as 'AnnualSalary' from codeboard;
select salary||''||contact  from codeboard;
select concat(empId,empNo) as 'joins' from codeboard;

alter table codeboard add designation varchar(40);
update codeboard set designation="developer" where empNo in(100,101,102,103,104);
 
 select max(salary) from codeboard;
 select min(salary) from codeboard;
 select avg(salary) from codeboard;
 select count(salary) from codeboard;
 commit;
  rollback;
 alter table codeboard drop  designation;
 rollback;
 SELECT 
    *
FROM
    codeboard;
 rollback;
select * from codeboard order by salary desc limit 3;

SELECT 
    *
FROM
    codeboard
ORDER BY empNo DESC
LIMIT 3;

commit;
select * from company;
delete from company;
rollback;

drop table company;


create table company (
eName varchar(20),
empId int(13) ,
empNo int(4),
salary int(5),
contact int(10));

insert into company values 
('sam' ,111,100,25000,9292992),
('samy' ,222,101,35000,9292991),
('gokul' ,333,102,45000,9293392),
('guna',444,103,55000,9292992),
('arun' ,555,104,65000,9292992);

select * from company;

SELECT 
    *
FROM
    company
ORDER BY empNo DESC
LIMIT 3;

select * from company;
select eName,empId,empNo,salary,contact ,salary*12 as annualsalary from company; 

insert into company values ('mathi',666,105,75000,23213123);
delete from company where eName='mathi';

alter table company drop contact;

select * from company where salary>10000 and eName like '%m%';
